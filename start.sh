#!/bin/sh
php composer.phar dump-autoload 
php artisan config:cache 
chown -R root:www-data /var/www/html
chmod -R 777 /var/www/html
service cron start 
exec apachectl -D FOREGROUND